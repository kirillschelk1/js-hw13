/*
    1.setTimeout виконує лише один цикл, а setInteval виконує постійний цикл
    2.так спрацює митьєво, бо немає ніякої затримки
    3.для того щоб рано чи пізно зупинити функцію setInterval
*/



const img = document.querySelectorAll(".images-wrapper .image-to-show")
let currentImg = 0
let interval = setInterval(nextImg, 3000);
function nextImg() {
    img[currentImg].className = 'image-to-show';
    currentImg = (currentImg+1)%img.length;
    img[currentImg].className = 'image-to-show active';
}
let playing = true;
let pauseButton = document.getElementById('pause');
let playButton = document.getElementById('play')
function pauseSlideshow() {
    playing = false;
    clearInterval(interval);
}
function playSlideshow() {
    playing = true;
    interval = setInterval(nextImg,3000);
}
pauseButton.onclick = function() {
    pauseSlideshow();
};
playButton.onclick = function(){
    playSlideshow()
}